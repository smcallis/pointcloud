# Pointcloud Demonstration Program

This is a very simple demo program showing SimpleX usage.  It parses and displays pointcloud data three dimensionally using nothing more
than standard X11 via the SimpleX library, with just a sprinkling of matrix math.  

It doesn't read any of the standard file formats out there for pointcloud data, since a file format parser wasn't the goal of the demo.
Instead it reads binary files which are expected to contain a 32-bit unsigned integer as the first entry indicating how many points the file
contains, followed by three floats and a 32-bit unsigned int for each point.  The three floats represent the x, y, and z coordinates of each
point respectively, and the unsigned integer is interpreted as the bottom 24 bits containing 8-bit RGB color values.

Several example files are available in the dat directory.  The meeting_room.dat file in particular has > 3 million points in it and thus 
is a good demonstration of the performance achieved.



