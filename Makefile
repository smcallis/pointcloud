override CC     := g++
override CCOPTS := -ansi -pedantic -Wall -Wextra -Iinc/ -O3 $(CCOPTS)
override LDOPTS :=

all: bin/pointcloud

bin/pointcloud: src/pointcloud.cc inc/simplex.h
	$(CC) $(CCOPTS) $< -lX11 -lm -o $@ 

# Cleanup and rebuild targets
clean: 
	@rm -f bin/*

remake: clean all
