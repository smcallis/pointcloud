/*******************************************************************************
 * Copyright (c) 2012 Sean McAllister
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *******************************************************************************/

#include <simplex.h>

#include <cmath>
#include <time.h>
#include <ctype.h>
#include <unistd.h>

typedef struct {
    double  x,y,z;
    pixel_t c;
} dpoint_t;

typedef struct {
    double el[4][4];
} mat4x4_t;

typedef struct {
    dpoint_t *points;      // raw data points
    size_t   npoint;      // number of points
    dpoint_t *points_proj; // buffer for projected points
} data_t;

// should be in math.h, sheesh
double radians(double x) { return x * M_PI/180.0; }
double degrees(double x) { return x * 180.0/M_PI; }


/*******************************************************************************
 * UI Definitions
 *******************************************************************************/
const double   MAX_ZOOM      = 5.0;
const double   MIN_ZOOM      = 0.25;
const uint16_t MARGIN        = 5;   // distance between UI components
const uint16_t STATUS_HEIGHT = 10;

typedef struct {
    xwin_t   win;          // ui window
    pixbuf_t pb;           // pixbuffer to draw to
    int16_t  ww,hh;        // window width/height
    bool     dragging;
    double  *zbuf;         // depth buffer
    
    rect_t plotrect;       // rectangle representing plot.
    rect_t statrect;       // status area

    // current and last rotation matrix and zoom levels
    double   zoom,lzoom;
    mat4x4_t rotmat, lrotmat; 
    dpoint_t  svec;         // starting vector in trackball
    
    // bounds of viewing volume
    double xmin,ymin,zmin,mmin;
    double xmax,ymax,zmax,mmax;

    mat4x4_t invproj;      // inverse projection matrix
    data_t   data;         // actual data points
} ui_t;


/*******************************************************************************
 * Matrix related stuff
 *******************************************************************************/

// Multiply a matrix and a point
static dpoint_t mat_vec_mul(mat4x4_t mat, dpoint_t vec) {
    dpoint_t ans = {
        mat.el[0][0]*vec.x + mat.el[0][1]*vec.y + mat.el[0][2]*vec.z + mat.el[0][3],
        mat.el[1][0]*vec.x + mat.el[1][1]*vec.y + mat.el[1][2]*vec.z + mat.el[1][3],
        mat.el[2][0]*vec.x + mat.el[2][1]*vec.y + mat.el[2][2]*vec.z + mat.el[2][3],
        vec.c
    };
    return ans;
}


/*******************************************************************************
 * Multiply an array of points by a projection/transformation matrix to put them
 * into device coordinates (NDC).  
 *
 * @param matrix  4x4 matrix, note the last row is unused.
 * @param dst     array of 3D points to store transformed pixels.
 * @param src     array of 3D points to project.
 * @param len     length of two input arrays.
 *******************************************************************************/
static void project_points(mat4x4_t matrix, dpoint_t *dst, dpoint_t *src, size_t len) {
    for (size_t ii=0; ii < len; ii++) {
        dst[ii] = mat_vec_mul(matrix, src[ii]);
    }
}


/*******************************************************************************
 * Generate an orthographic projection matrix that maps a retangular prism defined
 * by the given clipping plans to the normalized cube circumscribed by the unit
 * sphere.
 *
 * @param xl/xh  clipping plans in x dimension
 * @param yl/yh  clipping plans in y dimension
 * @param zl/zh  clipping plans in z dimension
 *******************************************************************************/
static mat4x4_t gen_ortho_matrix(double xl, double xh,
                                 double yl, double yh,
                                 double zl, double zh, double zoom) {

    mat4x4_t result; memset(&result, 0, sizeof(mat4x4_t));
    result.el[0][0] = +2/(xh-xl)/zoom;  result.el[0][3] = -(xh+xl)/(xh-xl)/zoom;
    result.el[1][1] = -2/(yh-yl)/zoom;  result.el[1][3] = +(yh+yl)/(yh-yl)/zoom;
    result.el[2][2] = -2/(zh-zl)/zoom;  result.el[2][3] = +(zh+zl)/(zh-zl)/zoom;
    result.el[3][3] = 1;
    return result;
}


/*******************************************************************************
 * turn a given vector into a unit vector
 *******************************************************************************/
dpoint_t unit_vec (dpoint_t a) {
    double  mag = hypot(a.x, hypot(a.y, a.z));
    dpoint_t ans = {a.x/mag, a.y/mag, a.z/mag, a.c};
    return  ans;
}


/*******************************************************************************
 * compute cross-product between two vectors
 *******************************************************************************/
dpoint_t vec_cross(dpoint_t a, dpoint_t b) {
    dpoint_t ans = {+(a.y*b.z - a.z*b.y),
                   -(a.x*b.z - a.z*b.x),
                   +(a.x*b.y - a.y*b.x), rgb_color(0)};
    return ans;
}


/*******************************************************************************
 * Computes A*B and returns the result.  Takes 4x4 matrices but only computes
 * 4x3 sub-product, presuming the last row of the transformation matrix to be 1
 *******************************************************************************/
static mat4x4_t mat_mul_4x4(mat4x4_t aa, mat4x4_t bb) {
    mat4x4_t result;

    for (size_t row=0; row < 3; row++) {
        for (size_t col=0; col < 4; col++) {
            result.el[row][col] = 0.0;
            for (size_t ii=0; ii < 4; ii++) {
                result.el[row][col] += aa.el[row][ii] * bb.el[ii][col];
            }
        }
    }

    // Fill in bottom row
    result.el[3][0] = 0.0;
    result.el[3][1] = 0.0;
    result.el[3][2] = 0.0;
    result.el[3][3] = 1.0;

    return result;
}


/*******************************************************************************
 * Invert a 4x4 transform matrix.  This uses the fact that the last row is 0 0 0 1
 * to speed things up.
 *
 * Given that A = | M b |  where M is 3x3, b is 3x1, inv(A) = | inv(M)  -inv(M)*b |
 *                | 0 1 |                                     |   0         1     |
 *******************************************************************************/
static mat4x4_t mat_inv_4x4(mat4x4_t aa) {
    dpoint_t bb  = {aa.el[0][3], aa.el[1][3], aa.el[2][3], rgb_color(0)};
    double   det =                                                      \
        + aa.el[0][0] * (aa.el[1][1] * aa.el[2][2] - aa.el[1][2] * aa.el[2][1])
        - aa.el[0][1] * (aa.el[1][0] * aa.el[2][2] - aa.el[1][2] * aa.el[2][0])
        + aa.el[0][2] * (aa.el[1][0] * aa.el[2][1] - aa.el[1][1] * aa.el[2][0]);

    if (fabs(det) < 1e-6) {
        fprintf(stderr, "zero determinant on transform matrix\n");
        exit(EXIT_FAILURE);
    }

    mat4x4_t ret;
    ret.el[0][0] = (aa.el[1][1]*aa.el[2][2] - aa.el[1][2]*aa.el[2][1])/det;
    ret.el[0][1] = (aa.el[0][2]*aa.el[2][1] - aa.el[0][1]*aa.el[2][2])/det;
    ret.el[0][2] = (aa.el[0][1]*aa.el[1][2] - aa.el[0][2]*aa.el[1][1])/det;

    ret.el[1][0] = (aa.el[1][2]*aa.el[2][0] - aa.el[1][0]*aa.el[2][2])/det;
    ret.el[1][1] = (aa.el[0][0]*aa.el[2][2] - aa.el[0][2]*aa.el[2][0])/det;
    ret.el[1][2] = (aa.el[0][2]*aa.el[1][0] - aa.el[0][0]*aa.el[1][2])/det;

    ret.el[2][0] = (aa.el[1][0]*aa.el[2][1] - aa.el[1][1]*aa.el[2][0])/det;
    ret.el[2][1] = (aa.el[0][1]*aa.el[2][0] - aa.el[0][0]*aa.el[2][1])/det;
    ret.el[2][2] = (aa.el[0][0]*aa.el[1][1] - aa.el[0][1]*aa.el[1][0])/det;

    ret.el[0][3] = -(ret.el[0][0]*bb.x + ret.el[0][1]*bb.y + ret.el[0][2]*bb.z);
    ret.el[1][3] = -(ret.el[1][0]*bb.x + ret.el[1][1]*bb.y + ret.el[1][2]*bb.z);
    ret.el[2][3] = -(ret.el[2][0]*bb.x + ret.el[2][1]*bb.y + ret.el[2][2]*bb.z);

    ret.el[3][0] = 0.0;
    ret.el[3][1] = 0.0;
    ret.el[3][2] = 0.0;
    ret.el[3][3] = 1.0;

    return ret;
}


// Modify transform matrix appropriate to simulate multiplying by a scale matrix
static mat4x4_t scale    (mat4x4_t mat, double xs, double ys, double zs) {
    for (size_t ii=0; ii < 4; ii++) { mat.el[0][ii] *= xs; }
    for (size_t ii=0; ii < 4; ii++) { mat.el[1][ii] *= ys; }
    for (size_t ii=0; ii < 4; ii++) { mat.el[2][ii] *= zs; }
    return mat;
}


// Modify transform matrix appropriate to simulate multiplying by a transform matrix
static mat4x4_t translate(mat4x4_t mat, double xt, double yt, double zt) {
    mat.el[0][3] += xt;
    mat.el[1][3] += yt;
    mat.el[2][3] += zt;
    return mat;
}


/*******************************************************************************
 * Return a constant identity matrix
 *******************************************************************************/
static mat4x4_t matrix_eye() {
    const static mat4x4_t mat={{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}}};
    return mat;
}


/*******************************************************************************
 * Return a rotation matrix that will rotate a given vector into another.
 *******************************************************************************/
static mat4x4_t rotate_to(dpoint_t a, dpoint_t b) {
    const double ROT_ACCEL = 2.0; // to make rotation feel a little snappier

    if (a.x == b.x && a.y == b.y && a.z == b.z) {
        // same vector, so return identity, this avoids instability in the cross product
        return matrix_eye();
    }
    
    dpoint_t u     = unit_vec(vec_cross(a,b));                     // axis of rotation
    double  angle = -ROT_ACCEL*acos(a.x*b.x + a.y*b.y + a.z*b.z); // assumes |a||b| == 1; 
    
    const double tcos  = cos(angle);
    const double tsin  = sin(angle);

    mat4x4_t ans  = {{                                                  
            {u.x*u.x*(1-tcos)+    tcos, u.x*u.y*(1-tcos)-u.z*tsin,  u.x*u.z*(1-tcos)+u.y*tsin, 0},
            {u.y*u.x*(1-tcos)+u.z*tsin, u.y*u.y*(1-tcos)+    tcos,  u.y*u.z*(1-tcos)-u.x*tsin, 0},
            {u.z*u.x*(1-tcos)-u.y*tsin, u.z*u.y*(1-tcos)+u.x*tsin,  u.z*u.z*(1-tcos)+    tcos, 0},
            {0,                         0,                          0,                         1}}};
    return ans;
}


/*******************************************************************************
 * Layout components of the GUI
 *******************************************************************************/
static void ui_layout(ui_t *ui) {
    ui->statrect = rectangle(MARGIN, ui->hh - STATUS_HEIGHT - MARGIN, ui->ww-2*MARGIN, STATUS_HEIGHT);
    ui->plotrect = rectangle(MARGIN, MARGIN,                          ui->ww-2*MARGIN, ui->statrect.y0 - 2*MARGIN);
}


/*******************************************************************************
 * Initalize the UI to a default state.
 *
 * @param ui     ui_t instance to initialize
 * @param ww     window width
 * @param hh     window height
 * @param top    window top
 * @param left   window left
 * @param files  array of pathnames to load
 * @param nfile  number of pathnames in paths
 *******************************************************************************/
static void ui_init(ui_t *ui,
                    uint16_t ww,  uint16_t hh,
                    uint16_t top, uint16_t left,
                    const char* file) {
    // Open X11 window at default size
    ui->ww       = ww;
    ui->hh       = hh;
    ui->dragging = false;
    xwin_init (&ui->win, 0, ui->ww, ui->hh, left, top);
    xwin_title(&ui->win, "Point Cloud");

    // Create pixbuf and depth buffer
    if (pixbuf_init(&ui->pb, ui->ww, ui->hh) != 0) {
        fprintf(stderr, "error creating pixbuffer\n");
        exit(EXIT_FAILURE);
    }
    ui->zbuf = (double*)malloc(ww*hh*sizeof(double));
    
    // Clear member variables
    ui->zoom   = ui->lzoom   = 1.0;          // full screen
    ui->rotmat = ui->lrotmat = matrix_eye(); // no rotation
    
    // Read in files
    uint32_t nsample=0;
    ui->data.points      = NULL;
    ui->data.points_proj = NULL;

    // Prime for min/max values
    ui->xmin   = ui->ymin    = ui->zmin = ui->mmin =  INFINITY;
    ui->xmax   = ui->ymax    = ui->zmax = ui->mmax = -INFINITY;

    FILE* fd = fopen(file, "rb");
    if (fd == NULL) {
        fprintf(stderr, "Error opening input file '%s'\n", file);
        exit(EXIT_FAILURE);
    }

    // read number of samples
    if (fread(&nsample, sizeof(uint32_t), 1, fd) != 1) {
        fprintf(stderr, "Error reading number of records from input\n"); exit(-1);
    } else {
        printf("Displaying %i samples\n", nsample);
    }

    // read data
    ui->data.points      = (dpoint_t*)realloc(ui->data.points,      nsample*sizeof(dpoint_t));
    ui->data.points_proj = (dpoint_t*)realloc(ui->data.points_proj, nsample*sizeof(dpoint_t));

    for (size_t ii=0; ii < nsample; ii++) {
        dpoint_t pnt;
        float x,y,z; uint32_t rgb;
        if ((fread(&x,   sizeof(float),    1, fd) != 1) ||
            (fread(&y,   sizeof(float),    1, fd) != 1) ||
            (fread(&z,   sizeof(float),    1, fd) != 1) ||
            (fread(&rgb, sizeof(uint32_t), 1, fd) != 1)) {

            fprintf(stderr, "Error parsing input\n");
            exit(-1);            
        }
        
        pnt.x = (double)x;
        pnt.y = (double)y;
        pnt.z = (double)z;
        pnt.c = rgb_color(rgb);
        
        ui->data.points[ii] = pnt;
        ui->xmin = fmin(pnt.x, ui->xmin); ui->xmax = fmax(pnt.x, ui->xmax);
        ui->ymin = fmin(pnt.y, ui->ymin); ui->ymax = fmax(pnt.y, ui->ymax);
        ui->zmin = fmin(pnt.z, ui->zmin); ui->zmax = fmax(pnt.z, ui->zmax);
    }
    
    fclose(fd);

    ui->data.npoint = nsample;

    // Layout ui
    ui_layout   (ui);
}


/*******************************************************************************
 * Redraw the plot
 *******************************************************************************/
static void ui_redraw_plot(ui_t *ui) {
    // clear depth buffer
    size_t zblen = ui->ww*ui->hh;
    for (size_t ii=0; ii < zblen; ii++) {
        ui->zbuf[ii] = INFINITY;        
    }

    pixbuf_set_clip  (&ui->pb, ui->plotrect);
    pixbuf_clear_rect(&ui->pb, rgb_color(0x000000), ui->plotrect.x0, ui->plotrect.y0, rect_width(ui->plotrect), rect_height(ui->plotrect));

    // Generate projection matrix, rotate and translate to get into screen coordinates
    mat4x4_t proj_matrix;
    proj_matrix = gen_ortho_matrix(ui->xmin, ui->xmax,
                                   ui->ymin, ui->ymax,
                                   ui->zmin, ui->zmax, ui->zoom);

    proj_matrix = mat_mul_4x4(ui->rotmat, proj_matrix);
    proj_matrix =     scale(proj_matrix, 700/2.0, 700/2.0, 1.0);
    proj_matrix = translate(proj_matrix,
                            (rect_width (ui->plotrect)-1)/2.0 + ui->plotrect.x0,
                            (rect_height(ui->plotrect)-1)/2.0 + ui->plotrect.y0, 0);

    // Generate inverse transformation matrix
    ui->invproj = mat_inv_4x4(proj_matrix);

    // Project points
    project_points(proj_matrix, ui->data.points_proj, ui->data.points, ui->data.npoint);

    // Draw points
    for (size_t ii=0; ii < ui->data.npoint; ii++) {
        int16_t xx = (uint16_t)lrint(ui->data.points_proj[ii].x);
        int16_t yy = (uint16_t)lrint(ui->data.points_proj[ii].y);
        double  zz = ui->data.points_proj[ii].z;

        if (point_in_rect(xx, yy, ui->plotrect)) {
            if (zz < ui->zbuf[yy*ui->ww+xx]) {
                draw_pixel(&ui->pb, ui->data.points_proj[ii].c, xx, yy);
                ui->zbuf[yy*ui->ww+xx] = zz;
            }
        }
    }

    // Draw plot borders
    draw_rect(&ui->pb,  rgb_color(0xFFFFFF), ui->plotrect, 1);

    // Define end-points of coordinate axis, and project them to get them into view space
    dpoint_t lines[6] = {{ ui->xmin,  ui->ymin,  ui->zmin, rgb_color(0)},
                        { ui->xmax,  ui->ymin,  ui->zmin, rgb_color(0)},

                        { ui->xmin,  ui->ymin,  ui->zmin, rgb_color(0)},
                        { ui->xmin,  ui->ymax,  ui->zmin, rgb_color(0)},

                        { ui->xmin,  ui->ymin,  ui->zmin, rgb_color(0)},
                        { ui->xmin,  ui->ymin,  ui->zmax, rgb_color(0)}};

    
    dpoint_t lines_proj[6]; project_points(proj_matrix, lines_proj, lines, 6);

    for (size_t ii=0; ii < 6; ii +=2) {
        draw_line(&ui->pb, rgb_color(0xBBBBBB), lines_proj[ii].x, lines_proj[ii].y, lines_proj[ii+1].x, lines_proj[ii+1].y);
    }

    // Draw labels
    const char* labels[3] = {"X+", "Y+", "Z+"};
    for (size_t ii=1; ii < 6; ii += 2) {
        // unit vector pointing along each axis to offset label by a bit
        dpoint_t uvec = {lines_proj[ii].x-lines_proj[ii-1].x,
                        lines_proj[ii].y-lines_proj[ii-1].y,
                        lines_proj[ii].z-lines_proj[ii-1].z, rgb_color(0)};
        double  umag = hypot(uvec.x, hypot(uvec.y, uvec.z));

        draw_text(&ui->pb, rgb_color(0xEEEEEE), lines_proj[ii].x + 10*uvec.x/umag, lines_proj[ii].y + 10*uvec.y/umag, labels[(ii-1)/2]);
    }
   
    // Refresh plot area
    pixbuf_redraw (&ui->pb, ui->plotrect);
    pixbuf_no_clip(&ui->pb);
}


/*******************************************************************************
 * Take the current mouse coordinate and generate a vector pointing to the surface
 * of either the unit sphere or a hyperbolic sheet centered in the image.
 * The hyperbolic sheet allows smoothly transitioning at edges of sphere.
 * see: http://www.opengl.org/wiki/Trackball
 *******************************************************************************/
static dpoint_t mouse_to_tball(ui_t *ui, event_t mpoint) {
    // in the plot, compute X and Y in normalize coodinates (ie -1 to 1 in each dim)
    double  xrad  =  rect_width(ui->plotrect)/2.0;
    double  yrad  = rect_height(ui->plotrect)/2.0;
    double  xcent = (double)ui->plotrect.x0 +  rect_width(ui->plotrect)/2.0;
    double  ycent = (double)ui->plotrect.y0 + rect_height(ui->plotrect)/2.0;

    double x = (mpoint.x-xcent)/xrad;
    double y = (mpoint.y-ycent)/yrad;
    double z = 0.0;

    // at r/2 transition to a hyperbolic surface rather than spherical to ease the transition
    if (hypot(x,y) >= 1.0/sqrt(2)) {
        z = 0.5/hypot(x,y);
    } else {
        z  = sqrt(1-(x*x+y*y));
    }
    
    dpoint_t ans = {x, y, z, rgb_color(0)};
    return  unit_vec(ans);
}


void help_and_quit(char *argv[], int status) {
    fprintf(stderr,
            "Usage: %s [options] [<input files>]\n%s", argv[0],
            ""
            );
    exit(status);
}


int main(int argc, char* argv[]) {
    uint16_t ww=800, hh=800;
    uint16_t top=25, left=25;

    if (argc < 2) {
        help_and_quit(argv, EXIT_FAILURE);
    }

    ui_t ui;
    ui_init      (&ui, ww, hh, top, left, argv[1]);
    ui_redraw_plot(&ui);
    pixbuf_redraw(&ui.pb, rectangle(0, 0, ww, hh));

    bool running = true;
    while (running) {
        event_t evt;
        if (xwin_event(&ui.win, &evt)) {
            switch(evt.type) {
            case WINDOW_RESIZE: {
                ui.ww = evt.w;
                ui.hh = evt.h;

                // Resize pixbuffers, also zeros memory
                pixbuf_size  (&ui.pb, ui.ww, ui.hh);
                ui.zbuf = (double*)realloc(ui.zbuf, ui.ww*ui.hh*sizeof(double));
                
                // Relayout and redraw all
                ui_layout     (&ui);
                ui_redraw_plot(&ui);
                pixbuf_redraw (&ui.pb, rectangle(0, 0, ww, hh));
                break;
            }

            case MOUSE_DRAG: {
                if (!ui.dragging) {
                    if (point_in_rect(evt.x, evt.y, ui.plotrect)) {
                        ui.dragging = true;
                        ui.svec     = mouse_to_tball(&ui, evt);
                    };
                } else {
                    if (evt.m == 1) {
                        // update rotation matrix and redraw
                        dpoint_t cvec = mouse_to_tball(&ui, evt);
                        ui.rotmat     = mat_mul_4x4(rotate_to(ui.svec, cvec), ui.lrotmat);
                       
                        ui_redraw_plot(&ui);
                    } else if (evt.m == 2) {
                        ui.zoom = ui.lzoom - evt.h * .03125;
                        ui.zoom = fmax(MIN_ZOOM, ui.zoom);
                        ui.zoom = fmin(MAX_ZOOM, ui.zoom);
                        ui_redraw_plot(&ui);
                    }
                }
            }

            case MOUSE_MOVE:
                break;

            case MOUSE_UP: {
                if (ui.dragging) {
                    ui.dragging = false;
                    if (evt.m == 1) {
                        ui.lrotmat = ui.rotmat;
                    } else if (evt.m == 2) {
                        ui.lzoom   = ui.zoom;
                    }
                }
                break;
            }


            case WINDOW_CLOSE:
            case KEY_PRESS: {
                running = false;
                break;
            }

            default:
                break;
            }
        } else {
            usleep(10000); // good for 100FPS
        }

        xwin_composite(&ui.win, &ui.pb);
    }
}
